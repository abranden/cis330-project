#include <ncurses.h>
#include <iostream>
#include <string>
#include <vector>
#include <string.h> // for memset
#include "ui.hpp"

#define CURSES_RED 1
#define CURSES_CYAN 2
#define CURSES_GREEN 3
#define CURSES_YELLOW 4
#define CURSES_MAGENTA 5

#define BUF_LENGTH 512

ChatScreen::ChatScreen() {
    /* if (has_colors() != TRUE) {
        std::cerr << "Your shell doesn't support colors?\n";
        std::cerr << "What year is it again?";
        std::cerr << std::endl;
        exit(9);
    }*/
    initscr(); // init ncurses
    //keypad(); // init keypad reading
    //start_color();
    //use_default_colors();
    cbreak();
    echo();
    // Create named color pairs with the default background color (-1):
    //init_pair(CURSES_RED, COLOR_RED, -1);
    //init_pair(CURSES_CYAN, COLOR_CYAN, -1);
    //init_pair(CURSES_GREEN, COLOR_GREEN, -1);
    //init_pair(CURSES_YELLOW, COLOR_YELLOW, -1);
    //init_pair(CURSES_MAGENTA, COLOR_MAGENTA, -1);
    getmaxyx(stdscr, _max_y, _max_x); // Get the size of the terminal window


    // Create the two windows:
    _display = newwin(_max_y - 1, _max_x, 0, 0);
    _entry = newwin(1, _max_x, _max_y - 1, 0);
    nodelay(_display, FALSE);
    scrollok(_display, TRUE);
    wmove(_entry, 0, 0);
    wrefresh(_display);
    wrefresh(_entry);
}

void ChatScreen::writeToDisplay(std::string message) {
    std::string tempString;
    char buffer[_max_x + 1];
    bool have_printed_a_line = false;
    while (message.length() > 0) {
        // While there's still some message left, chop off one line's worth of
        // message and print it

        memset(buffer, '\0', _max_x + 1); // Clear the buffer
        // Get a "line" worth of text from the front of the string (i.e.
        // _max_x - 1 characters, since the terminal is max_x wide)
        // if we are continuing from a previous line, indent by four spaces:
        if (have_printed_a_line) {
            message = "    " + message;
        }
        have_printed_a_line = true;
        tempString = message.substr(0, _max_x - 1);
        tempString.push_back('\n');
        // Remove the string that we just got from message:
        message.erase(0, _max_x - 1);
        // Put the string we just grabbed into the buffer:
        tempString.copy(buffer, _max_x, 0);
        // Scroll the window up one line:
        wscrl(_display, 1);
        wrefresh(_display);
        // Print to the bottom of the screen the line that we just got:
        mvwprintw(_display, _max_y - 3, 0, buffer);
    }
    wrefresh(_display);
    wmove(_entry, 0, 0);
    wrefresh(_entry);
}

std::string ChatScreen::getUserInput() {
    std::string userInput;
    char buffer[BUF_LENGTH] = {};
    mvwgetnstr(_entry, 0, 0, buffer, BUF_LENGTH-1);
    userInput = std::string(buffer);
    wclear(_entry);
    return userInput;
}

void ChatScreen::clearDisplay() {
    werase(_display);
    wrefresh(_display);
}
