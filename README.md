# README #

### How do I get set up? ###

* run git clone https://bitbucket.org/abranden/cis330-project.git
* run "make build"
* for server run "./chatServer (portnum)" (e.g. ./chatServer 6500)
* for client run "./chatClient (server hostname) (portnum)" (e.g. ./chatClient ix.cs.uoregon.edu 6500)